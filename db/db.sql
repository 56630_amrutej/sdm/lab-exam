--  MySQL and create Movie(movie_id, movie_title, movie_release_date,movie_time,director_name) table 


CREATE TABLE movie(
    movie_id integer primary key auto_increment,
    movie_title varchar(50),
    movie_release_date DATE,
    movie_time FLOAT,
    director_name varchar(50)
);

INSERT INTO movie (movie_title, movie_release_date,movie_time,director_name) VALUES ('a','2021-1-1',2,'a');
INSERT INTO movie (movie_title, movie_release_date,movie_time,director_name) VALUES ('b','2021-1-2',2,'b');
INSERT INTO movie (movie_title, movie_release_date,movie_time,director_name) VALUES ('c','2021-1-3',2,'c');
INSERT INTO movie (movie_title, movie_release_date,movie_time,director_name) VALUES ('d','2021-1-4',2,'d');
INSERT INTO movie (movie_title, movie_release_date,movie_time,director_name) VALUES ('e','2021-1-5',2,'e');
INSERT INTO movie (movie_title, movie_release_date,movie_time,director_name) VALUES ('f','2021-1-6',2,'f');