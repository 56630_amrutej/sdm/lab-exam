const express= require('express')
const db= require('../db')
const utils= require('../utils')


const router= express.Router()

router.get('/',(request,response)=>{

const {name}= request.body

const query= `
select * from movie where movie_title='${name}'
`
db.execute(query,(error,result)=>{
    response.send(utils.createResult(error,result))
})
})


router.post('/',(request,response)=>{

    const {movie_title, movie_release_date, movie_time, director_name}= request.body
    
    const query= `
    INSERT INTO movie (movie_title, movie_release_date, movie_time, director_name) 
    VALUES ('${movie_title}','${movie_release_date}','${movie_time}','${director_name}')
    `
    db.execute(query,(error,result)=>{
        response.send(utils.createResult(error,result))
    })
    })



    router.put('/:id',(request,response)=>{

        // Update Release_Date and Movie_Time 
        const{id}= request.params
        const {movie_release_date, movie_time }= request.body
        
        const query= `
        UPDATE movie SET 
         movie_release_date= '${movie_release_date}',
         movie_time= '${movie_time}' 
        WHERE
        movie_id= '${id}'
        `
        db.execute(query,(error,result)=>{
            response.send(utils.createResult(error,result))
        })
        })





        router.delete('/:id',(request,response)=>{
            const{id}= request.params
            
            const query= `
            DELETE FROM movie 
            WHERE
            movie_id= '${id}'
            `
            db.execute(query,(error,result)=>{
                response.send(utils.createResult(error,result))
            })
            })











module.exports= router